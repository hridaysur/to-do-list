//check of specific todos by clicking

$("ul").on ("click","li", function(){
	$(this).toggleClass("completed");
});

//click on X to delet to-do

$('ul').on ("click","span", function(event){
	$(this).parent().fadeOut(500, function(){
		$(this).remove();
	});
	event.stopPropagation();
})

$("input[type='text']").keypress(function(event){
	if(event.which === 13){
		//grabing new tod text
		var todoText = $(this).val();
		$(this).val("");
		//creat new li
		$('ul').append("<li><span><i class='far fa-trash-alt'></i> </span>"+ todoText +"</li>");
	}
})

$("#toggle-form").click(function(){
	$("input[type='text']").fadeToggle();
});